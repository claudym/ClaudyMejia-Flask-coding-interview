import json
from flask import Flask
from schemas import UsersSchema

app = Flask(__name__)
users_schema = UsersSchema(many=True)

@app.route("/")
def main():
    response = {
        'message': 'Welcome to the BEON Python/Django Challenge'
    }
    return response, 200


@app.route("/users")
def users():
    with open('data/users.json') as f:
        data = json.load(f)
        # print(data)
    return users_schema.load(data), 200



@app.route("/categories")
def categories():
    response = {
        'message': 'Nothing build yet.'
    }
    return response, 404


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
