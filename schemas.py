from marshmallow import Schema, fields, validates, ValidationError
from datetime import datetime, date


class UsersSchema(Schema):
    class Meta:
        ordered = True
    
    # birth_date = fields.Date(format='%Y-%m-%d')
    birth_date = fields.Str(required=True)
    email = fields.Email(required=True)
    first_name = fields.Str(required=False)
    gender = fields.Str(required=True)
    id = fields.Int(required=False)
    ip_address = fields.Str(required=False)
    last_name = fields.Str(required=False)

    @validates("birth_date")
    def validate_birth_date(self, value):
        today = date.today()
        delta = (datetime.strptime(value, '%Y-%m-%d').date() - today) // 365
        if delta <= 18:
            raise ValidationError('Only adults.')
